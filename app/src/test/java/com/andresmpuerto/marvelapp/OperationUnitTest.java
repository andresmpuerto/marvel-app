package com.andresmpuerto.marvelapp;

import com.andresmpuerto.marvelapp.utils.Utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class OperationUnitTest {
  
  @Test
  public void operation_isCorrect() {
    String text = "1 + ( -2)";
    Object obj = 0;
    if (Utils.isValid(text)) {
      obj = Utils.operation(text);
    }
    int num = -1;
    if (obj instanceof Double){
      num = (int) ((Double) obj).doubleValue();
    }else if (obj instanceof Integer){
      num = (int) obj;
    }
    
    assertEquals(text + " is a valid operation.",-1, num);
  }
  
  @Test
  public void multiple_isCorrect() {
    assertTrue("30 is multiple of 3", Utils.isMultiple(30, 3) );
  }
  
  @Test
  public void multiple_isFail() {
    assertFalse("30 does not multiple of 7", Utils.isMultiple(30, 7) );
  }
  
}