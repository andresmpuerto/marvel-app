package com.andresmpuerto.marvelapp.utils;


/**
 * @author andresmpuerto.
 *
 */
public class Constantes {
  
  
  // Delay
  public static final int SPLASH_SCREEN_DELAY = 800;
  
  //URL BASE
  public static final String BASE_URL = "https://gateway.marvel.com/";
  
  public static final String PARAMETER_KEY = "apikey=";
  public static final String TS_KEY = "ts=";
  public static final String HASH_KEY = "hash=";
  
  public static final String TS_VALUE = "1";
  // Obtenido de md5(ts+privateKey+publicKey) - https://developer.marvel.com/documentation/authorization
  public static final String HASH_VALUE = "b714f204efab89b88890974bee6766d2";
  
  
  // ENDPOINTS
  public static final String CHARACTERS = "v1/public/characters";
  public static final String COMICS = "v1/public/comics";
  public static final String CREATORS = "v1/public/creators";
  public static final String EVENTS = "v1/public/events";
  public static final String SERIES = "v1/public/series";
  public static final String STORIES = "v1/public/stories";
  
  //API KEY MARVEL
  public static final String PUBLIC_KEY = "c93a0acd8938ff496552f7555dda5279";
  public static final String PRIVATE_KEY = "9a6d8ed52e112bb749a0cbbb8d9709e89f6eb26d";
  
  
  public static final String KEY_CHARACTER = "Character";
  public static final String KEY_COMIC = "Comic";
  public static final String KEY_CREATOR = "Creator";
  public static final String KEY_EVENT = "Event";
  public static final String KEY_SERIE = "Serie";
  public static final String KEY_STORY = "Story";
  
  
  
  

}
