package com.andresmpuerto.marvelapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Created by Lmartinez on 08/11/2016.
 */
public class Utils {
  
  public static String KEY = "?" + Constantes.PARAMETER_KEY + Constantes.PUBLIC_KEY + "&" +
      Constantes.TS_KEY + Constantes.TS_VALUE + "&" + Constantes.HASH_KEY + Constantes.HASH_VALUE;
  
  public static boolean isConnected(Context context) {
    ConnectivityManager connMgr = (ConnectivityManager)
        context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo;
    if (connMgr != null) {
      activeNetworkInfo = connMgr.getActiveNetworkInfo();
      return (activeNetworkInfo != null &&
          activeNetworkInfo.isConnected() && activeNetworkInfo.isAvailable());
    }
    return false;
  }
  
  public static String encodeText(String a) {
    MessageDigest digest;
    String hash = "";
    try {
      digest = MessageDigest.getInstance("MD5");
      digest.update(a.getBytes());
      hash = bytesToHexString(digest.digest());
    } catch (NoSuchAlgorithmException e1) {
      e1.printStackTrace();
    }
    return hash;
    
  }
  
  private static String bytesToHexString(byte[] bytes) {
    StringBuilder sb = new StringBuilder();
    for (byte aByte : bytes) {
      String hex = Integer.toHexString(0xFF & aByte);
      if (hex.length() == 1) {
        sb.append('0');
      }
      sb.append(hex);
    }
    return sb.toString();
  }
  
  
  public void  generateHash(){
    encodeText("1" + Constantes.PUBLIC_KEY + Constantes.PRIVATE_KEY);
  }
  
  
  public static boolean isValid(CharSequence operacion) {
    String reg = "((-)?([0-9]+)|(([-+])[(]( |-| -)?)([0-9]+[)]))( )?([-+*/])( )?(((-)?[0-9]+)|(( -| +)[(]( |-| -)?)([0-9]+[)]))";
    Pattern sPattern = Pattern.compile(reg);
    return sPattern.matcher(operacion).matches();
  }
  
  /**
   * Realiza un eval de la operacion desde un String
   * @param pattern operacion validada
   * @return Object con el resultado de la operación ingresada
   */
  public static Object operation(String pattern){
    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    try {
      Object result = engine.eval(pattern);
      System.out.println(pattern + " = " + result);
      return result;
    } catch (ScriptException se) {
      se.printStackTrace();
      return null;
    }
  }
  
  public static boolean isMultiple(int num, int multiple){
    return num % multiple == 0;
  }
  
  
}