package com.andresmpuerto.marvelapp.service.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author andresmpuerto
 * @since 10/25/18.
 */
public class Project implements  Serializable{
  
  
  @SerializedName("code")
  public int code;
  @SerializedName("status")
  public String status;
  @SerializedName("copyright")
  public String copyright;
  @SerializedName("attributionText")
  public String attributionText;
  @SerializedName("attributionHTML")
  public String attributionHTML;
  @SerializedName("etag")
  public String etag;
  @SerializedName("data")
  public Data data;
  
  public class Data{
    @SerializedName("offset")
    public int offset;
    @SerializedName("limit")
    public int limit;
    @SerializedName("total")
    public int total;
    @SerializedName("count")
    public int count;
    @SerializedName("results")
    public List<Results> results;
    
    public Data() {
    }
  }
  
  public class Results implements Serializable{
    
    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name = null;
    @SerializedName("description")
    public String description;
    @SerializedName("modified")
    public String modified;
    @SerializedName("resourceURI")
    public String resourceURI;
    @SerializedName("thumbnail")
    public Image thumbnail;
    @SerializedName("title")
    public String title = null;
    @SerializedName("fullName")
    public String fullName;
    @SerializedName("type")
    public String type = null;
    
    public Results() {
    }
    
    public class Image {
      @SerializedName("path")
      public String path;
      @SerializedName("extension")
      public String extension;
      
      public Image() {
      }
    }
  }
}

