package com.andresmpuerto.marvelapp.service.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.andresmpuerto.marvelapp.service.model.Project;

import com.andresmpuerto.marvelapp.utils.Constantes;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author andresmpuerto
 * @since 10/22/18.
 */
public class MarvelRepository {
  
  private MarvelService marvelService;
  private static MarvelRepository marvelRepository;
  
  public MarvelRepository() {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(Constantes.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build();
    
    marvelService = retrofit.create(MarvelService.class);
  }
  
  public synchronized static MarvelRepository getInstance() {
    if (marvelRepository == null) {
      marvelRepository = new MarvelRepository();
    }
    return marvelRepository;
  }
  
  
  public LiveData<Project> getCharacterList(){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getCharacterList().enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getComicList(){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getComicList().enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getCreatorList(){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getCreatorList().enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
  
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getEventList(){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getEventList().enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getSerieList(){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getSerieList().enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getStoryList(){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getStoryList().enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  
  public LiveData<Project> getCharacterDetail(int id){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getCharacterDetail(id).enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        Log.d("uuuu", response.code()+" pp");
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getComicDetail(int id){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getComicDetail(id).enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getCreatorDetail(int id){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getCreatorDetail(id).enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getEventDetail(int id){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getEventDetail(id).enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getSerieDetail(int id){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getSerieDetail(id).enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
  
  public LiveData<Project> getStoryDetail(int id){
    final MutableLiveData<Project> data = new MutableLiveData<>();
    marvelService.getStoryDetail(id).enqueue(new Callback<Project>() {
      @Override
      public void onResponse(Call<Project> call, Response<Project> response) {
        data.setValue(response.body());
      }
      
      @Override
      public void onFailure(Call<Project> call, Throwable t) {
        Log.d("uuuu", t.getMessage());
        data.setValue(null);
      }
    });
    
    return data;
  }
}
