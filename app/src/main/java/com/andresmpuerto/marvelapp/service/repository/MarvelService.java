package com.andresmpuerto.marvelapp.service.repository;

import com.andresmpuerto.marvelapp.service.model.Project;

import com.andresmpuerto.marvelapp.utils.Constantes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * @author andresmpuerto
 * @since 10/22/18.
 */
public interface MarvelService {
  
  String KEY = "?" + Constantes.PARAMETER_KEY + Constantes.PUBLIC_KEY + "&" +
      Constantes.TS_KEY + Constantes.TS_VALUE + "&" + Constantes.HASH_KEY + Constantes.HASH_VALUE;
  
  @GET(Constantes.CHARACTERS + KEY)
  Call<Project> getCharacterList();
  
  @GET(Constantes.CHARACTERS + "/{id}" + KEY)
  Call<Project> getCharacterDetail(@Path("id") int id);
  
  @GET(Constantes.COMICS + KEY)
  Call<Project> getComicList();
  
  @GET(Constantes.COMICS + "/{id}" + KEY)
  Call<Project> getComicDetail(@Path("id") int id);
  
  @GET(Constantes.CREATORS + KEY)
  Call<Project> getCreatorList();
  
  @GET(Constantes.CREATORS + "/{id}" + KEY)
  Call<Project> getCreatorDetail(@Path("id") int id);
  
  @GET(Constantes.SERIES + KEY)
  Call<Project> getSerieList();
  
  @GET(Constantes.SERIES + "/{id}" + KEY)
  Call<Project> getSerieDetail(@Path("id") int id);
  
  @GET(Constantes.EVENTS + KEY)
  Call<Project> getEventList();
  
  @GET(Constantes.EVENTS + "/{id}" + KEY)
  Call<Project> getEventDetail(@Path("id") int id);
  
  @GET(Constantes.STORIES + KEY)
  Call<Project> getStoryList();
  
  @GET(Constantes.STORIES + "/{id}" + KEY)
  Call<Project> getStoryDetail(@Path("id") int id);
}
