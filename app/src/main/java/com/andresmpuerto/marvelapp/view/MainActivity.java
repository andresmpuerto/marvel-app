package com.andresmpuerto.marvelapp.view;

import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.widget.SearchView;

import com.andresmpuerto.marvelapp.R;;
import com.andresmpuerto.marvelapp.service.model.Project;
import com.andresmpuerto.marvelapp.utils.Constantes;
import com.andresmpuerto.marvelapp.utils.Utils;
import com.andresmpuerto.marvelapp.view.adapter.AdapterCards;
import com.andresmpuerto.marvelapp.viewmodel.ViewModelMarvel;

import java.io.Serializable;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{
  
  private ViewModelMarvel viewModelMarvel;
  private AdapterCards<Project.Results> projectAdapterCards;
  private List<Project.Results> projectResults;
  private String type = "";
  
  //------------------------
  
  
  private SearchManager searchManager;
  private SearchView searchView;
  
  @BindView(R.id.cards)
  RecyclerView cards;
  
  @BindView(R.id.input_operacion)
  EditText input_operation;
  
  @BindView(R.id.btn_calculate)
  Button calculate;
  
  @BindView(R.id.result)
  TextView result;
  
  @BindView(R.id.progressBar1)
  ProgressBar loader;
  
  Snackbar snackbar;
  
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    viewModelMarvel = new ViewModelMarvel();
    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT);
    
    mLayoutManager.generateLayoutParams(lp);
    
    cards.setLayoutManager(mLayoutManager);
    
  }
  
  @OnClick(R.id.btn_calculate)
  public void calculate(View view) {
    String operation = input_operation.getText().toString();
    
    if (TextUtils.isEmpty(operation)) {
      Snackbar.make(view,
          "The field can't be empty.",
          Snackbar.LENGTH_SHORT)
          .show();
    } else {
      if (Utils.isConnected(this)) {
        if (snackbar != null && snackbar.isShown()){
          snackbar.dismiss();
        }
        makeCalculate(operation);
        loader.setVisibility(View.VISIBLE);
        cards.setVisibility(View.GONE);
      } else{
        snackbar = Snackbar.make(view,
            "Network Failed..",
            Snackbar.LENGTH_INDEFINITE)
            .setAction("Retry", new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                calculate(view);
              }
            });
        snackbar.show();
      }
      
    }
  }
  
  
  public void makeCalculate(String pattern) {
    if (Utils.isValid(pattern)) {
      Object obj = Utils.operation(pattern);
      
      if (obj != null) {
        int num = -1;
        if (obj instanceof Double) {
          num = (int) ((Double) obj).doubleValue();
        } else if (obj instanceof Integer) {
          num = (int) obj;
        }
        
        if (num == 0) {
          result.setText(String.valueOf(num));
          viewModelMarvel.getCharacterList();
          observeViewModel(viewModelMarvel);
          type = Constantes.KEY_CHARACTER;
        } else if (num >= 3) {
          if (Utils.isMultiple(num, 3)) {
            viewModelMarvel.getComicList();
            type = Constantes.KEY_COMIC;
          } else if (Utils.isMultiple(num, 5)) {
            viewModelMarvel.getComicList();
            type = Constantes.KEY_COMIC;
          } else if (Utils.isMultiple(num, 7)) {
            viewModelMarvel.getCreatorList();
            type = Constantes.KEY_CREATOR;
          } else if (Utils.isMultiple(num, 11)) {
            viewModelMarvel.getEventList();
            type = Constantes.KEY_EVENT;
          } else if (Utils.isMultiple(num, 13)) {
            viewModelMarvel.getSerieList();
            type = Constantes.KEY_SERIE;
          } else {
            viewModelMarvel.getStoryList();
            type = Constantes.KEY_STORY;
          }
          result.setText(String.valueOf(num));
        } else {
          result.setText(String.valueOf(num));
          viewModelMarvel.getStoryList();
          type = Constantes.KEY_STORY;
        }
        observeViewModel(viewModelMarvel);
      } else {
        result.setText(R.string.error_text);
        viewModelMarvel.getStoryList();
        type = Constantes.KEY_STORY;
        observeViewModel(viewModelMarvel);
      }
    } else {
      result.setText(R.string.error_text);
      viewModelMarvel.getStoryList();
      type = Constantes.KEY_STORY;
      observeViewModel(viewModelMarvel);
    }
  }
  
  private void observeViewModel(final ViewModelMarvel viewModel) {
    viewModel.getObservableProject().observe(this, new Observer<Project>() {
      @Override
      public void onChanged(@Nullable Project project) {
        if (project != null) {
          if (projectAdapterCards == null) {
            showCards(project);
          }else{
            projectAdapterCards.updateData(project.data.results);
          }
          loader.setVisibility(View.GONE);
          cards.setVisibility(View.VISIBLE);
        }
      }
    });
  }
  
  public void showCards(Project response) {
    projectAdapterCards = new AdapterCards<>(response.data.results, AdapterCard);
    cards.setAdapter(projectAdapterCards);
  }
  
  public AdapterCards.OnItemClickListener AdapterCard = new AdapterCards.OnItemClickListener() {
    @Override
    public void onItemClick(Serializable item) {
      Intent intent = new Intent(MainActivity.this, ActivityDetail.class);
      intent.putExtra("ID_OBJECT", ((Project.Results) item).id);
      intent.putExtra("TYPE_OBJECT", type);
      startActivity(intent);
    }
  };
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.home, menu);
    
    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
    searchView.setMaxWidth(Integer.MAX_VALUE);
    searchView.setQueryHint(getString(R.string.search_text));
    searchView.setOnQueryTextListener(this);
    
    return super.onCreateOptionsMenu(menu);
  }
  
  @Override
  public boolean onQueryTextSubmit(String query) {
    searchView.setQuery("", false);
    searchView.setIconified(true);
    projectAdapterCards.getFilter().filter(query);
    projectAdapterCards.notifyDataSetChanged();
    return false;
  }
  
  @Override
  public boolean onQueryTextChange(String newText) {
    projectAdapterCards.getFilter().filter(newText);
    projectAdapterCards.notifyDataSetChanged();
    return false;
  }
}
