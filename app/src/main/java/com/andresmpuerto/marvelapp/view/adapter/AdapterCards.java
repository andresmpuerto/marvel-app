package com.andresmpuerto.marvelapp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.andresmpuerto.marvelapp.R;
import com.andresmpuerto.marvelapp.service.model.Project;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author andresmpuerto
 * @since 10/22/18.
 */
public class AdapterCards<T> extends RecyclerView.Adapter<AdapterCards.ViewHolder>
    implements Filterable {
  
  private List<T> list_model;
  private OnItemClickListener listener;
  private List<T> appsFiltered;
  
  
  public AdapterCards(List<T> list_model, OnItemClickListener listener) {
    this.list_model = list_model;
    this.listener = listener;
    this.appsFiltered = list_model;
  }
  
  @NonNull
  @Override
  public AdapterCards.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_cards, parent, false);
    ViewHolder vh = new ViewHolder(v);
    vh.setContext(parent.getContext());
    return vh;
  }
  
  @Override
  public void onBindViewHolder(@NonNull AdapterCards.ViewHolder holder, int position) {
    holder.setItems(appsFiltered.get(position), this.listener);
  }
  
  
  @Override
  public Filter getFilter() {
    return new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence charSequence) {
        String charString = charSequence.toString();
        if (charString.isEmpty()) {
          appsFiltered = list_model;
        } else {
          List<T> filteredList = new ArrayList<>();
          for (T row : list_model) {
            if (row instanceof Project.Results) {
  
              Project.Results results = ((Project.Results) row);
              if (results.name != null) {
                if (results.name.toLowerCase().contains(charString.toLowerCase())) {
                  filteredList.add(row);
                }
              }else{
                if (results.title.toLowerCase().contains(charString.toLowerCase())) {
                  filteredList.add(row);
                }
              }
            }
          }
          appsFiltered = filteredList;
        }
        FilterResults filterResults = new FilterResults();
        filterResults.values = appsFiltered;
        return filterResults;
      }
      
      @Override
      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        appsFiltered = (ArrayList<T>) filterResults.values;
        notifyDataSetChanged();
      }
  
      @Override
      public CharSequence convertResultToString(Object resultValue) {
        Log.d("uuuu", resultValue.toString());
        return super.convertResultToString(resultValue);
      }
    };
  }
  @Override
  public int getItemCount() {
    return appsFiltered.size();
  }
  
  
  public void updateData(List<T> data){
    if (appsFiltered != null){
      appsFiltered.clear();
      appsFiltered.addAll(data);
    } else{
      appsFiltered = data;
    }
    notifyDataSetChanged();
  }
  
  class ViewHolder extends RecyclerView.ViewHolder {
    
    private Context context;
    
    @BindView(R.id.imagen_card)
    ImageView image;
    
    @BindView(R.id.title_card)
    TextView title;
    
    
    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
    
    public void setContext(Context context) {
      this.context = context;
    }
    
    
    public void setItems(final T model, final OnItemClickListener listener) {
      
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          listener.onItemClick((Serializable) model);
        }
      });
      
      if (model instanceof Project.Results){
        
        Project.Results results = ((Project.Results) model);
        
        if (results.thumbnail != null) {
          setImage(results.thumbnail.path + "/landscape_large." + results.thumbnail.extension);
        }
        
        if (results.name != null){
          title.setText( results.name );
        } else if (results.title != null){
          title.setText( results.title );
        }else {
          title.setText( results.fullName );
        }
        
      }
      
    }
  
    public void setImage(String url){
      Picasso.get().load(url).into(new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
          image.setImageBitmap(bitmap);
        }
      
        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
        
        }
      
        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        
        }
      });
    }
  }
  
  public interface OnItemClickListener {
    void onItemClick(Serializable item);
  }
  
}
