package com.andresmpuerto.marvelapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.andresmpuerto.marvelapp.R;
import com.andresmpuerto.marvelapp.utils.Constantes;

import java.util.Timer;
import java.util.TimerTask;

public class ActivitySplash extends AppCompatActivity {
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.splash);
    
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
          startActivity(new Intent(ActivitySplash.this, MainActivity.class));
          finish();
      }
      
    };
    
    // Simulate a long loading process on application startup.
    Timer timer = new Timer();
    timer.schedule(task, Constantes.SPLASH_SCREEN_DELAY);
  }
}
