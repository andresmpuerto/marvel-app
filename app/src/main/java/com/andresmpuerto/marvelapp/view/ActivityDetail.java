package com.andresmpuerto.marvelapp.view;

import android.arch.lifecycle.Observer;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.andresmpuerto.marvelapp.R;
import com.andresmpuerto.marvelapp.service.model.Project;
import com.andresmpuerto.marvelapp.utils.Constantes;
import com.andresmpuerto.marvelapp.viewmodel.ViewModelMarvelDetail;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityDetail extends AppCompatActivity {
  
  @BindView(R.id.image)
  ImageView image;
  
  @BindView(R.id.title)
  TextView title;
  
  @BindView(R.id.description)
  TextView description;
  
  
  private ViewModelMarvelDetail viewModelMarvelDetail;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    ButterKnife.bind(this);
  
    if(getActionBar() != null){
      getActionBar().setDisplayHomeAsUpEnabled(true);
    }
  
    if(getSupportActionBar() != null){
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    Bundle extras = getIntent().getExtras();
    
    int id = extras.getInt("ID_OBJECT");
    String type = extras.getString("TYPE_OBJECT");
    
    viewModelMarvelDetail = new ViewModelMarvelDetail();
    
    switch (type){
      case Constantes.KEY_CHARACTER:
        viewModelMarvelDetail.getCharacterDetail(id);
        break;
        
      case Constantes.KEY_COMIC:
        viewModelMarvelDetail.getComicDetail(id);
        break;
        
      case Constantes.KEY_CREATOR:
        viewModelMarvelDetail.getCreatorDetail(id);
        break;
        
      case Constantes.KEY_EVENT:
        viewModelMarvelDetail.getEventDetail(id);
        break;
        
      case Constantes.KEY_SERIE:
        viewModelMarvelDetail.getSerieDetail(id);
        break;
        
      case Constantes.KEY_STORY:
        viewModelMarvelDetail.getStoryDetail(id);
        break;
    }
  
    observeViewModel(viewModelMarvelDetail);
    
  }
  
  private void observeViewModel(final ViewModelMarvelDetail viewModel) {
    viewModel.getObservableProjectResult().observe(this, new Observer<Project>() {
      @Override
      public void onChanged(@Nullable Project project) {
        if (project != null) {
          Project.Results results = project.data.results.get(0);
          Log.d("yyy", "oo"+ results.id);
          if (results.thumbnail != null) {
            setImage(results.thumbnail.path + "/landscape_large." + results.thumbnail.extension);
          }
  
          if (results.name != null){
            title.setText( results.name );
          } else if (results.title != null){
            title.setText( results.title );
          }else {
            title.setText( results.fullName );
          }
          
          if (results.description != null && !results.description.isEmpty()){
            description.setText(results.description);
          } else{
            description.setText(R.string.no_description);
          }
        }
      }
    });
  }
  
  
  public void setImage(String url){
    Picasso.get().load(url).into(new Target() {
      @Override
      public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        image.setImageBitmap(bitmap);
      }
      
      @Override
      public void onBitmapFailed(Exception e, Drawable errorDrawable) {
      
      }
      
      @Override
      public void onPrepareLoad(Drawable placeHolderDrawable) {
      
      }
    });
  }
  
  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}
