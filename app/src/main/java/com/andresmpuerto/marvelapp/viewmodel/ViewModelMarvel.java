package com.andresmpuerto.marvelapp.viewmodel;

import android.arch.lifecycle.LiveData;


import com.andresmpuerto.marvelapp.service.model.Project;

import com.andresmpuerto.marvelapp.service.repository.MarvelRepository;


/**
 * @author andresmpuerto
 * @since 10/22/18.
 */
public class ViewModelMarvel {
  
  private LiveData<Project> projectLiveData;
  
  public ViewModelMarvel() {
  
  }
  
  public LiveData<Project> getObservableProject() {
    return projectLiveData;
  }
  
  
  //-------------------------------
  
  public void getCharacterList(){
    projectLiveData = MarvelRepository.getInstance().getCharacterList();
  }
  
  
  public void getComicList(){
    projectLiveData = MarvelRepository.getInstance().getComicList();
    
  }
  
  public void getCreatorList(){
    projectLiveData = MarvelRepository.getInstance().getCreatorList();
    
  }
  
  public void getEventList(){
    projectLiveData = MarvelRepository.getInstance().getEventList();
    
  }
  
  public void getSerieList(){
    projectLiveData = MarvelRepository.getInstance().getSerieList();
    
  }
  
  public void getStoryList(){
    projectLiveData = MarvelRepository.getInstance().getStoryList();
  }
  
  
  
}
