package com.andresmpuerto.marvelapp.viewmodel;

import android.arch.lifecycle.LiveData;

import com.andresmpuerto.marvelapp.service.model.Project;
import com.andresmpuerto.marvelapp.service.repository.MarvelRepository;


/**
 * @author andresmpuerto
 * @since 10/22/18.
 */
public class ViewModelMarvelDetail {
  
  private LiveData<Project> projectLiveData;
  
  public ViewModelMarvelDetail() {
  
  }
  
  
  public LiveData<Project> getObservableProjectResult() {
    return projectLiveData;
  }
  
  //-------------------------------
  
  public void getCharacterDetail(int id){
    projectLiveData = MarvelRepository.getInstance().getCharacterDetail(id);
    
  }
  
  
  public void getComicDetail(int id){
    projectLiveData = MarvelRepository.getInstance().getComicDetail(id);
    
  }
  
  public void getCreatorDetail(int id){
    projectLiveData = MarvelRepository.getInstance().getCreatorDetail(id);
    
  }
  
  public void getEventDetail(int id){
    projectLiveData = MarvelRepository.getInstance().getEventDetail(id);
    
  }
  
  public void getSerieDetail(int id){
    projectLiveData = MarvelRepository.getInstance().getSerieDetail(id);
    
  }
  
  public void getStoryDetail(int id){
    projectLiveData = MarvelRepository.getInstance().getStoryDetail(id);
  }
}
